from bs4 import BeautifulSoup
import requests


if __name__ == "__main__":
    base_url = "https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview"


    soup = BeautifulSoup(requests.get(base_url).text, "html.parser")

    bodies = soup("html")

    for body in bodies:
        for img in body("img"):
            img.decompose()

    with open("index.html", "w", encoding="utf-8") as f:
        f.write(str(bodies))

